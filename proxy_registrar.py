import socket
import sys
import socketserver
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import json
import time
import random
import hashlib

FICHERO = sys.argv[1]


class ProxHandler(ContentHandler):
    lista = []
    diccionario = {}

    def __init__(self):
        self.name = ""
        self.passwdpath = ""
        self.ip = ""
        self.puerto = ""
        self.path = ""
        self.diccionario = {}
        self.lista = []

    def startElement(self, name, attrs):

        def add_info():
            self.lista.append(self.diccionario)
            self.diccionario = {}

        if name == "server":
            self.diccionario["name"] = attrs.get("name", "")
            self.diccionario["ip"] = attrs.get("ip", "")
            self.diccionario["puerto"] = attrs.get("puerto", "")
            add_info()
        elif name == "database":
            self.diccionario["path"] = attrs.get("path", "")
            self.diccionario["passwdpath"] = attrs.get("passwdpath", "")
            add_info()
        elif name == "log":
            self.diccionario["log_path"] = attrs.get("path", "")
            add_info()

    def get_info(self):
        return self.lista


def log(contenido):
    tiempo_actual = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
    contenido = contenido.replace("\r\n", " ")
    fichero = open(log_file, "a")
    fichero.write(tiempo_actual + " " + contenido + "[...]" + "\r\n")
    fichero.close


class EchoHandler(socketserver.DatagramRequestHandler):
    """
    Echo server class
    """
    lista = []
    diccionario = {}
    passdicc = {}
    dicc_dig = {}

    def register2json(self, file):
        with open(file, "w") as salida:
            json.dump(self.diccionario, salida, indent=1)

    def json2registered(self, file):
        try:
            with open(file, "r") as archivo_datos:
                self.diccionario = json.load(archivo_datos)
        except:
            pass

    def passwd(self, file):
        try:
            with open(file, "r") as archivo_datos:
                self.passdicc = json.load(archivo_datos)
        except:
            pass

    def expirado(self):
        lista_exp = []
        tiempo = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
        for user in self.diccionario:
            if tiempo > self.diccionario[user][1]:
                lista_exp.append(user)
        for user in lista_exp:
            del self.diccionario[user]

    def handle(self):
        self.json2registered(database_path)
        llegada = self.rfile.read()
        linea_log = llegada.decode("utf-8")
        linea = llegada.decode("utf-8").split()
        print(linea)
        user = linea[1].split(":")[1]
        puerto = lista[0]['puerto']
        if linea[0] == "INVITE" or linea[0] == "ACK" or linea[0] == "BYE":
            if user not in self.diccionario:
                self.wfile.write(b"SIP/2.0 404 User Not Found" + b"\r\n\r\n")
                # Escribimos en el log
                contenido = "Received from " + ip + ":" + str(puerto) + ": " + linea_log
                log(contenido)
                contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "404 User Not Found "
                log(contenido)
            else:
                puerto = self.diccionario[user][2]
                with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    my_socket.connect((ip, puerto))
                    my_socket.send(llegada)
                    data = my_socket.recv(1024)
                    self.wfile.write(data)

        elif linea[0] == "CANCEL" or linea[0] == "OPTIONS" or linea[0] == "PRACK" or linea[0] == "SUBSCRIBE" or linea[0] == "NOTIFY" or linea[0] == "PUBLISH" or linea[0] == "INFO" or linea[0] == "REFER" or linea[0] == "MESSAGE" or linea[0] == "UPDATE":
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed" + b"\r\n\r\n")
            # Escribimos en el log
            contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "405 Method Not Allowed "
            log(contenido)

        elif linea[0] == "REGISTER":
            print(linea[-1])
            if linea[-1] == str(0):
                print("Usuario " + user + " eliminado")
                del self.diccionario[user]
                self.register2json(database_path)
                self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n\r\n")
                # Escribimos en el log
                contenido = "Received from " + ip + ":" + str(puerto) + ": " + linea_log
                log(contenido)
                contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "200 OK "
                log(contenido)
            elif user in self.diccionario:
                print("Usuario " + user + " encontrado")
                self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n\r\n")
                # Escribimos en el log
                contenido = "Received from " + ip + ":" + str(puerto) + ": " + linea_log
                log(contenido)
                contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "200 OK "
                log(contenido)
            else:
                if len(linea) == 5:
                    print("Usuario " + user + " no autorizado")
                    numero = str(random.randint(000000, 100000))
                    self.dicc_dig[user] = numero
                    self.wfile.write(b"SIP/2.0 401 Unauthorized" + b"\r\n"
                    + b"WWW Authenticate: Digest nonce=" + bytes(numero, 'utf-8') + b"\r\n\r\n")
                    # Escribimos en el log
                    contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "401 Unauthorized" + " " + "WWW Authenticate: Digest nonce=" + numero + " "
                    log(contenido)
                elif len(linea) == 8:
                    respons = linea[7].split("=")[1]
                    print(respons[1])
                    f = "%Y-%m-%d %H:%M:%S"
                    expires = int(linea[4])
                    tiempo = time.time() + expires
                    exp_time = time.strftime(f, time.localtime(tiempo))
                    puerto = int(linea[1].split(":")[2])
                    self.passwd(password_path)
                    password = self.passdicc[user]
                    hash = hashlib.md5()
                    num = self.dicc_dig[user]
                    hash.update(bytes(num, 'utf-8'))
                    hash.update(bytes(password, 'utf-8'))
                    hash.digest()
                    diggest = hash.hexdigest()
                    print(diggest)
                    print(self.dicc_dig[user])
                    if diggest == respons:
                        userlist = [ip, exp_time, puerto]
                        self.diccionario[user] = userlist
                        self.expirado()
                        print(self.diccionario)
                        self.register2json(database_path)
                        self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n\r\n")
                        # Escribimos en el log
                        contenido = "Received from " + ip + ":" + str(puerto) + ": " + linea_log
                        log(contenido)
                        contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "200 OK "
                        log(contenido)
        elif linea[0] != "INVITE" or linea[0] != "ACK" or linea[0] != "BYE" or linea[0] != "REGISTER":
            self.wfile.write(b"SIP/2.0 400 Bad Request" + b"\r\n\r\n")
            # Escribimos en el log
            contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "400 Bad Request "
            log(contenido)


if __name__ == "__main__":
    try:
        FICHERO = sys.argv[1]
    except ValueError:
        sys.exit("Usage: python proxy_registrar.py config")

    parser = make_parser()
    handler = ProxHandler()
    parser.setContentHandler(handler)
    parser.parse(open(FICHERO))
    lista = handler.get_info()

    server_name = lista[0]['name']
    ip = lista[0]['ip']
    puerto = lista[0]['puerto']
    database_path = lista[1]['path']
    password_path = lista[1]['passwdpath']
    log_file = lista[2]['log_path']

    serv = socketserver.UDPServer((ip, int(puerto)), EchoHandler)

    print("Server " + server_name + " listening at port " + puerto + "...")
    serv.serve_forever()
