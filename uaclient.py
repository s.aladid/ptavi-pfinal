import socket
import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import hashlib
import os
import time


class UAclient(ContentHandler):
    lista = []
    diccionario = {}

    def __init__(self):
        self.username = ""
        self.passwd = ""
        self.ip = ""
        self.puerto = ""
        self.path = ""
        self.diccionario = {}
        self.lista = []

    def startElement(self, name, attrs):

        def add_info():
            self.lista.append(self.diccionario)
            self.diccionario = {}

        if name == "account":
            self.diccionario["username"] = attrs.get("username", "")
            self.diccionario["passwd"] = attrs.get("passwd", "")
            add_info()
        elif name == "uaserver":
            self.diccionario["ua_ip"] = attrs.get("ip", "")
            self.diccionario["ua_puerto"] = attrs.get("puerto", "")
            add_info()
        elif name == "rtpaudio":
            self.diccionario["rtp_puerto"] = attrs.get("puerto", "")
            add_info()
        elif name == "regproxy":
            self.diccionario["prox_ip"] = attrs.get("ip", "")
            self.diccionario["prox_puerto"] = attrs.get("puerto", "")
            add_info()
        elif name == "log":
            self.diccionario["log_path"] = attrs.get("path", "")
            add_info()
        elif name == "audio":
            self.diccionario["audio_path"] = attrs.get("path", "")
            add_info()

    def get_info(self):
        return self.lista


def log(contenido):
    tiempo_actual = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
    contenido = contenido.replace("\r\n", " ")
    fichero = open(log_file, "a")
    fichero.write(tiempo_actual + " " + contenido + "[...]" + "\r\n")
    fichero.close


if __name__ == "__main__":

    FICHERO = sys.argv[1]
    METODO = sys.argv[2]
    OPCION = sys.argv[3]

    parser = make_parser()
    handler = UAclient()
    parser.setContentHandler(handler)
    try:
        parser.parse(open(FICHERO))
    except IOError:
        sys.exit("Fichero config no encontrado")
    lista = handler.get_info()

    usuario = lista[0]["username"]
    contraseña = lista[0]["passwd"]
    ip = lista[1]["ua_ip"]
    puerto = lista[1]["ua_puerto"]
    rtp_port = lista[2]["rtp_puerto"]
    proxy_IP = lista[3]["prox_ip"]
    proxy_port = lista[3]["prox_puerto"]
    log_file = lista[4]["log_path"]
    audio_file = lista[5]["audio_path"]

    if METODO == "REGISTER":
        envio = METODO + " sip:" + usuario + ":" + puerto + " SIP/2.0" + "\r\n" + "Expires: " + OPCION + "\r\n\r\n"
        # Escribimos en el log
        contenido = "Sent to " + ip + ":" + str(puerto) + ": " + str(METODO) + " sip:" + usuario + ":" + str(puerto) + " SIP/2.0 "
        log(contenido)
    elif METODO == "INVITE":
        print("Enviamos SDP")
        envio = METODO + " sip:" + OPCION + " SIP/2.0" + "\r\n" + "Content-Type: application/sdp" + "\r\n\r\n" + "v=0" + "\r\n" + "o=" + usuario + " " + ip + "\r\n" + "s=misesion" + "\r\n" + "t=0" + "\r\n" + "m=audio " + rtp_port + " RTP" + "\r\n\r\n"
        # Escribimos en el log
        contenido = "Sent to " + ip + ":" + str(puerto) + ": " + str(METODO) + " SIP/2.0 "
        log(contenido)
    elif METODO == "BYE":
        envio = METODO + " sip:" + usuario + "\r\n\r\n"
        # Escribimos en el log
        contenido = "Sent to " + ip + ":" + str(puerto) + ": " + str(METODO) + " SIP/2.0 "
        log(contenido)

    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
        my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        my_socket.connect((proxy_IP, int(proxy_port)))
        my_socket.send(bytes(envio, 'utf-8'))
        data = my_socket.recv(1024)
        linea_log = data.decode('utf-8')
        # Escribimos en el log
        contenido = "Received from " + ip + ":" + str(puerto) + ": " + linea_log
        log(contenido)

        if METODO == "REGISTER":
            if data.decode('utf-8').startswith("SIP/2.0 401 Unauthorized"):
                datos = data.decode('utf-8').split("\r\n")
                numero = datos[1].split("=")[1]
                hash = hashlib.md5()
                hash.update(bytes(numero, 'utf-8'))
                hash.update(bytes(contraseña, 'utf-8'))
                hash.digest()
                diggest = hash.hexdigest()
                my_socket.send(bytes(METODO, 'utf-8') + b" sip:" + bytes(usuario, 'utf-8') + b":" + bytes(puerto, 'utf-8') + b" SIP/2.0" +
                b"\r\n" + b"Expires: " + bytes(OPCION, "utf-8") + b"\r\n" + b"Authorization: Digest response=" + bytes(diggest, 'utf-8') + b"\r\n\r\n")
        elif METODO == "INVITE":
            if data.decode('utf-8').startswith("SIP/2.0 100 Trying"):
                my_socket.send(b"ACK sip:" + bytes(usuario, 'utf-8') + b" SIP/2.0" + b"\r\n\r\n")
                aEjecutar = "./mp32rtp -i " + ip + " -p " + rtp_port + " < " + audio_file
                print("Vamos a ejecutar ", aEjecutar)
                os.system(aEjecutar)
                print("Terminado de ejecutar")
                # Escribimos en el log
                contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "ACK sip:" + usuario + " SIP/2.0 "
                log(contenido)
                contenido = "Sent to " + ip + ":" + str(puerto) + ": " + str(aEjecutar) + " SIP/2.0 "
                log(contenido)

        print(data.decode('utf-8'))
        print("Terminando socket...")

    print("Fin.")
