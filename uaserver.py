
import socketserver
import os
import sys
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import time


class UAserver(ContentHandler):
    lista = []
    diccionario = {}

    def __init__(self):
        self.username = ""
        self.passwd = ""
        self.ip = ""
        self.puerto = ""
        self.path = ""
        self.diccionario = {}
        self.lista = []

    def startElement(self, name, attrs):

        def add_info():
            self.lista.append(self.diccionario)
            self.diccionario = {}

        if name == "account":
            self.diccionario["username"] = attrs.get("username", "")
            self.diccionario["passwd"] = attrs.get("passwd", "")
            add_info()
        elif name == "uaserver":
            self.diccionario["ua_ip"] = attrs.get("ip", "")
            self.diccionario["ua_puerto"] = attrs.get("puerto", "")
            add_info()
        elif name == "rtpaudio":
            self.diccionario["rtp_puerto"] = attrs.get("puerto", "")
            add_info()
        elif name == "regproxy":
            self.diccionario["prox_ip"] = attrs.get("ip", "")
            self.diccionario["prox_puerto"] = attrs.get("puerto", "")
            add_info()
        elif name == "log":
            self.diccionario["log_path"] = attrs.get("path", "")
            add_info()
        elif name == "audio":
            self.diccionario["audio_path"] = attrs.get("path", "")
            add_info()

    def get_info(self):
        return self.lista


def log(contenido):
    tiempo_actual = time.strftime('%Y%m%d%H%M%S', time.gmtime(time.time()))
    contenido = contenido.replace("\r\n", " ")
    fichero = open(log_file, "a")
    fichero.write(tiempo_actual + " " + contenido + "[...]" + "\r\n")
    fichero.close


class EchoHandler(socketserver.DatagramRequestHandler):

    def handle(self):
        linea = self.rfile.read().decode("utf-8").split()
        print(linea)
        llegada = self.rfile.read()
        linea_log = llegada.decode("utf-8")
        aEjecutar = "./mp32rtp -i " + ip + " -p " + rtp_port + " < " + audio_file
        if linea[0] == "INVITE":
            print("Nos llega invite")
            self.wfile.write(b"SIP/2.0 100 Trying" + b"\r\n\r\n" +
            b"SIP/2.0 180 Ringing" + b"\r\n\r\n" +
            b"SIP/2.0 200 OK" + b"\r\n\r\n" +
            b"Content-Type: application/sdp" + b"\r\n\r\n" +
            b"v=0" + b"\r\n" +
            b"o=" + bytes(usuario, 'utf-8') + b"" + bytes(ip, 'utf-8') + b"\r\n" +
            b"s=misesion" + b"\r\n" +
            b"t=0" + b"\r\n" +
            b"m=audio " + bytes(rtp_port, 'utf-8') + b" RTP" + b"\r\n\r\n")
            print("Envio de SDP")
            # Escribimos en el log
            contenido = "Received from " + ip + ":" + str(puerto) + ": " + linea_log
            log(contenido)
            contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "SIP/2.0 100 Trying" + "\r\n\r\n" + "SIP/2.0 180 Ringing" + "\r\n\r\n" + "SIP/2.0 200 OK"
            log(contenido)
        elif linea[0] == "ACK":
            print("Vamos a ejecutar ", aEjecutar)
            os.system(aEjecutar)
            print("Terminado de ejecutar")
            self.wfile.write(b"Audio acabado")
            # Escribimos en el log
            contenido = "Sent to " + ip + ":" + str(puerto) + ": " + str(aEjecutar) + " SIP/2.0 "
            log(contenido)
        elif linea[0] == "CANCEL" or linea[0] == "OPTIONS" or linea[0] == "PRACK" or linea[0] == "SUBSCRIBE" or linea[0] == "NOTIFY" or linea[0] == "PUBLISH" or linea[0] == "INFO" or linea[0] == "REFER" or linea[0] == "MESSAGE" or linea[0] == "UPDATE":
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed" + b"\r\n\r\n")
            # Escribimos en el log
            contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "405 Method Not Allowed "
            log(contenido)
        elif linea[0] == "BYE":
            self.wfile.write(b"SIP/2.0 200 OK" + b"\r\n\r\n")
            # Escribimos en el log
            contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "200 OK "
            log(contenido)
        elif linea[0] != "INVITE" or linea[0] != "ACK" or linea[0] != "BYE" or linea[0] != "REGISTER":
            self.wfile.write(b"SIP/2.0 400 Bad Request" + b"\r\n\r\n")
            # Escribimos en el log
            contenido = "Sent to " + ip + ":" + str(puerto) + ": " + "400 Bad Request "
            log(contenido)


if __name__ == "__main__":
    try:
        FICHERO = sys.argv[1]
    except ValueError:
        sys.exit("Usage: python uaserver.py config")

    parser = make_parser()
    handler = UAserver()
    parser.setContentHandler(handler)
    parser.parse(open(FICHERO))
    lista = handler.get_info()

    usuario = lista[0]["username"]
    contraseña = lista[0]["passwd"]
    ip = lista[1]["ua_ip"]
    puerto = lista[1]["ua_puerto"]
    rtp_port = lista[2]["rtp_puerto"]
    proxy_IP = lista[3]["prox_ip"]
    proxy_port = lista[3]["prox_puerto"]
    log_file = lista[4]["log_path"]
    audio_file = lista[5]["audio_path"]

    serv = socketserver.UDPServer((ip, int(puerto)), EchoHandler)
    print("Listening...")
    serv.serve_forever()
